/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;
import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author gilmo
 */
public class CSVUtils {

    // Num of columns in example file.
    private static final int MED_TABLE_NUM_OF_COLUMNS = 9;
    
    // skip labels row.
    private static final int MED_TABLE_FIRST_ROW_WITH_DATA = 1;
    
    private static final int MAX_ROWS_IN_FILE = 1400;
    
    public static ArrayList<MedicalTableEntry> readCSVTypeFile(String filename) {
        ArrayList<MedicalTableEntry> entries = null;
        if(filename.endsWith(".csv") || filename.endsWith(".CSV")) {
            entries = CSVUtils.ReadCSV(filename);
        }
        else if (filename.endsWith(".xls") || filename.endsWith(".xlsx")
                || filename.endsWith(".XLS") || filename.endsWith(".XLSX")) {
            entries = CSVUtils.readXLS(filename);
        }
        else {
            Utils.print("Unknows file type. not csv or xls or xlsx.");        
        }
        return entries;
        
    }
    
    public static ArrayList<MedicalTableEntry> readXLS(String filename) {
        ArrayList<MedicalTableEntry> table = new ArrayList<>();
        
        FileInputStream inputStream = null;
        Workbook workbook = null;
        Sheet sheet = null;
        try {
            inputStream = new FileInputStream(new File(filename));
        } catch (FileNotFoundException e) {
            Utils.print("File " + filename + " not found.");
            return null;
        }
        try {
            // return HSSF or XSSF for xls or xlsx files accordingly.
            workbook = getWorkbook(inputStream, filename);
        } catch (IOException e) {
            Utils.print("Error opening XSSWorkBook from file " + filename);
            return null;
        }
        try {
            sheet = workbook.getSheetAt(0);
        } catch (Exception e) {
            Utils.print("Error openeing sheet 0 in file " + filename);
            return null;
        }
        int rowNum = -1;
        int cn = -1;
        try {
            DataFormatter cellFormatter = new DataFormatter(); // for formatting all cells to plain text.
            int rowStart = MED_TABLE_FIRST_ROW_WITH_DATA;
            int rowEnd = Math.max(MAX_ROWS_IN_FILE, sheet.getLastRowNum());
             
            
            for (rowNum = rowStart; rowNum < rowEnd; rowNum++) {
                
                Row r = sheet.getRow(rowNum);
                if (r == null) {
                    continue;
                }
                ArrayList<String> values = new ArrayList<>();
                for (cn = 0; cn < MED_TABLE_NUM_OF_COLUMNS; cn++) {
                    Cell cell = r.getCell(cn);
                    if (cn == 0 && cell.toString().isEmpty()) {
                        break;
                    }
                    values.add(cellFormatter.formatCellValue(cell));
                }
                if(!values.isEmpty()) {
                    table.add(new MedicalTableEntry(values.toArray(new String[values.size()])));
                }
            }
        } catch (Exception e) {
            Utils.print("Error reading cells from xls file. row " + rowNum + " cell " + cn );
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                // Not the worse that can happen. Ignore.
            }
        }
        return table;
    }
    
    private static Workbook getWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException {

        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }
        return workbook;
    }

    public static ArrayList<MedicalTableEntry> ReadCSV(String filename) {
        ArrayList<MedicalTableEntry> table = new ArrayList<>();

        Scanner scanner;
        try {
            scanner = new Scanner(new File(filename));
        } catch (FileNotFoundException e) {
            Utils.print("file " + filename + " wasn't found");
            return null;
        }
        try {
            // get rid of first line with columns names.
            if (scanner.hasNextLine()) {
                scanner.nextLine();
            }

//            scanner.useDelimiter(",");
            while (scanner.hasNextLine()) {
                String l = scanner.nextLine();
                String[] line = l.split(",");
                if (line.length == 0 || line[0].isEmpty()) {
                    break;
                }
                table.add(new MedicalTableEntry(line));

            }
        } catch (Exception e) {
            Utils.print("Error reading CSV file " + filename);
            return null;
        } finally {
            scanner.close();
        }

        return table;

    }
}

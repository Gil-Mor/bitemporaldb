/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

import static bitemporaldb.MedicalTableEntry.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author gilmo
 */
public class UI {

    public static final int ILLEGAL_CHOICE                  = -1; // default random value.
    public static final int MIN_CHOICE                      = 1;
    public static final int INSERT_TO_TABLE_FROM_FILE       = 1;
    public static final int INSERT_TO_TABLE_MANUALLY        = 2;
    public static final int GET_ROW                         = 3;
    public static final int SHOW_PATIENT_PARAMETER_HISTORY  = 4;
    public static final int DELETE_FROM_TABLE               = 5;
    public static final int PRINT_MED_TABLE                 = 6;
    public static final int QUIT                            = 7;
    
    
    private static final Scanner scanner = new Scanner( System.in );
    
    
    public static ArrayList<String> options;

    public static void InitUI() {
        
        scanner.useDelimiter("[\n]");
        options = new ArrayList<>();
        options.add(0, null);
        options.add(INSERT_TO_TABLE_FROM_FILE, "Insert To Table From File");
        options.add(INSERT_TO_TABLE_MANUALLY, "Insert To Table Manually");
        options.add(GET_ROW, "Get Row");
        options.add(SHOW_PATIENT_PARAMETER_HISTORY, "Show Patient Paramter History Between Times");
        options.add(DELETE_FROM_TABLE, "Delete Row From Table");
        options.add(PRINT_MED_TABLE, "Print Med Records Table");
        options.add(QUIT, "Quit");
    }
    
    public static void finish() {
        scanner.close();
    }

    public static int getUserChoice() {
        displayOptions();
        return readUserChoice();
    }

    public static void displayOptions() {
        for (int i = MIN_CHOICE; i < options.size(); ++i) {
            Utils.printf("%d. %s", i, options.get(i));
        }
        Utils.print("Enter you choice: ");
    }

    public static int readUserChoice() {

        int c = ILLEGAL_CHOICE;

        try {
            while (true) {
                if (scanner.hasNextLine()) {
                    c = Integer.parseInt(scanner.nextLine());

                    if (c < MIN_CHOICE || options.size() <= c) {
                        Utils.printf("Error. choice should be an integer between %d and %d", MIN_CHOICE, options.size() - 1);
                        continue;
                    }
                    break;
                }               
            }
        } catch (InputMismatchException e) {
            Utils.print("Error reading int from user. Try again.");
        }
        return c;
    }

    public static MedicalTableEntry getEntryFromUser() {
        String[] entryVals = new String[NUM_OF_ENTRY_VALS];
        
        Utils.print("Enter first name: ");
        entryVals[firstNameIndex] = scanner.nextLine();
        
        Utils.print("Enter last name: ");
        entryVals[lastNameIndex] = scanner.nextLine();
        
        Utils.print("Enter parameter name: ");
        entryVals[parameterNameIndex] = scanner.nextLine();
        
        Utils.print("Enter value: ");
        entryVals[valueIndex] = scanner.nextLine();
        
        Utils.print("Enter unit: ");
        entryVals[unitIndex] = scanner.nextLine();
        
        Utils.print("Enter Valid Start Time <dd/MM/yyyy HH:mm> (enter \"now\" to insert current time): ");
        entryVals[validStartTimeIndex] = scanner.nextLine().trim();
        
        Utils.print("Enter Valid Stop Time <dd/MM/yyyy HH:mm> (enter \"now\" to insert current time): ");
        entryVals[validStopTimeIndex] = scanner.nextLine().trim();
        
        return new MedicalTableEntry(entryVals);
    }

    public static String getFileName() {
        Utils.print("Enter a cxs, xls or xlsx filename. The file should either be in BiTemporalDB folder\n or give a full path.");
        String filename = "";
        try {
            while(filename.isEmpty()) {
                if (scanner.hasNextLine()) {
                    filename = scanner.nextLine();
                    break;
                }
                
            }

        } catch (NoSuchElementException e) {
            Utils.print("Error getting filename String from user. Try again.");
        }
        catch (Exception e) {
            Utils.print(e.getMessage());
        }

        return filename;
    }
}

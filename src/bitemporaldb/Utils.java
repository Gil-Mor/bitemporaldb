/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

/**
 *
 * @author gilmo
 */
public class Utils {
    
    public static void print(String s) {
         System.out.println(s);
    }
    
    public static void printf(String format, Object... args) {
        Utils.print(String.format(format, args));
    }
}

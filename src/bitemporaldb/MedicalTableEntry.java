/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author gilmo
 */
public class MedicalTableEntry {
    
    public static final int firstNameIndex            = 0;
    public static final int lastNameIndex             = 1;
    public static final int parameterNameIndex        = 2;
    public static final int valueIndex                = 3;
    public static final int unitIndex                 = 4;
    public static final int validStartTimeIndex       = 5;
    public static final int validStopTimeIndex        = 6;
    public static final int TransactionTimeIndex      = 7;
    public static final int TransactionTimeStopIndex  = 8;
    
    public static final int NUM_OF_ENTRY_VALS         = 9;

    private String firstName;
    private String lastName;
    private String parameterName;
    private String value;
    private String unit;
    private Date validStartTime;
    private Date validStopTime;
    private Date TransactionStartTime;
    private Date TransactionStopTime;
    
    public static DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public MedicalTableEntry(String[] entryVals) { 
        setFirstName(entryVals[firstNameIndex]);
        setLastName(entryVals[lastNameIndex]);
        setParameterName(entryVals[parameterNameIndex]);
        setValue(entryVals[valueIndex]);
        setUnit(entryVals[unitIndex]);
        setValidStartTime(dateFromStr(entryVals[validStartTimeIndex]));
        setValidStopTime(dateFromStr(entryVals[validStopTimeIndex]));
        setTransactionStartTime(dateFromStr(entryVals[TransactionTimeIndex]));
        
        // No transaction stop time. Need this in case reading from csv file.
        if (entryVals.length >= TransactionTimeStopIndex)
            return;
        setTransactionStopTime(dateFromStr(entryVals[TransactionTimeStopIndex]));
        
    }

    public static Date dateFromStr(String dateStr) {
        if (null == dateStr || dateStr.isEmpty()) {
            return null;
        }
        try {
            if (dateStr.equalsIgnoreCase("now")) {
                return dateFormatter.parse(dateFormatter.format(new Date()));
            }
            return dateFormatter.parse(dateStr);
        } catch (ParseException e) {
            Utils.print("Error Parsing Date: " + dateStr + "\nFormat should be: " + dateFormatter.toString());
            
            // TODO: remove in production!
            System.exit(1);
        }
        catch (Exception e) {
            Utils.print("unknown exception in date formatter. date is " + dateStr);
            
            // TODO: remove in production!
            System.exit(1);
        }
        return null;
    }


    public String getFirstName() {
        return firstName;
    }

    public final void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public final void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParameterName() {
        return parameterName;
    }

    public final void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getValue() {
        return value;
    }

    public final void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }


    public final void setUnit(String unit) {
        this.unit = unit;
    }


    public Date getValidStartTime() {
        return validStartTime;
    }

    public final void setValidStartTime(Date validStartTime) {
        this.validStartTime = validStartTime;
    }

    public Date getValidStopTime() {
        return validStopTime;
    }

    public final void setValidStopTime(Date validStopTime) {
        this.validStopTime = validStopTime;
    }


    public Date getTransactionStartTime() {
        return TransactionStartTime;
    }

    public final void setTransactionStartTime(Date TransactionStartTime) {
        if (TransactionStartTime == null) {
            TransactionStartTime = new Date();
        }
        this.TransactionStartTime = TransactionStartTime;
    }


    public Date getTransactionStopTime() {
        return TransactionStopTime;
    }


    public final void setTransactionStopTime(Date TransactionStopTime) {
        this.TransactionStopTime = TransactionStopTime;
    }
    
    @Override
    public String toString() {
        String s = String.format("|| %s | %s | %s | %s | %s | %s|  %s | %s | ",
                firstName, lastName, parameterName, value, unit, 
                dateFormatter.format(validStartTime), dateFormatter.format(validStopTime), 
                dateFormatter.format(TransactionStartTime));
        if(null != TransactionStopTime) {
            s += String.format("%s ||",dateFormatter.format(TransactionStopTime));
        }
        else {
            s += String.format("%s ||",TransactionStopTime);
        }
        return s;
    }
    
    public String toStringExtended() {
        String s = String.format("First Name: %s. Last Name: %s. Parameter Name: %s. Value: %s. Unit: %s. "
                + "Valid Start Time: %s. Valid Stop Time: %s. Transaction Time: %s.",
                firstName, lastName, parameterName, value, unit, 
                dateFormatter.format(validStartTime), dateFormatter.format(validStopTime), 
                dateFormatter.format(TransactionStartTime));
        if(null != TransactionStopTime) {
            s += String.format(" Transaction Stop Time: %s.",dateFormatter.format(TransactionStopTime));
        }
        else {
            s += String.format(" Transaction Stop Time: %s.",TransactionStopTime);
        }
        return s;
    }

    
}

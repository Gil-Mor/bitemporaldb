/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author gilmo
 */
public class DB {

    // The connection to the database
    private Connection con = null;
    public String DBName = "BiTemporalDB";
    public String tableName = "MedRecords";
    public int MedRecordsSize = 0;
    
    public Date currentDate; 
    
    public DB() {
        currentDate = new Date(new java.util.Date().getTime());
    }

    public void InsertToTableFromFile(String filename) {
        ArrayList<MedicalTableEntry> entries = CSVUtils.readCSVTypeFile(filename);

        if (entries == null) {
            return;
        }
        for (MedicalTableEntry entry : entries) {
            insertEntry(entry);

        }
    }

    public void insertEntry(MedicalTableEntry entry) {
        PreparedStatement p;
        try {
            p = con.prepareStatement("INSERT INTO " + tableName + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ");
        } catch (SQLException e) {
            Utils.print("Error creating statement");
            return;
        }
        try {
            p.setString(1, entry.getFirstName());
            p.setString(2, entry.getLastName());
            p.setString(3, entry.getParameterName());
            p.setString(4, entry.getValue());
            p.setString(5, entry.getUnit());

            p.setTimestamp(6, javaUtilDateToSQLTimestamp(entry.getValidStartTime()));
            p.setTimestamp(7, javaUtilDateToSQLTimestamp(entry.getValidStopTime()));
            p.setTimestamp(8, javaUtilDateToSQLTimestamp(entry.getTransactionStartTime()));
            p.setTimestamp(9, javaUtilDateToSQLTimestamp(entry.getTransactionStopTime()));

            p.executeUpdate();
            //p.close();
            ++MedRecordsSize;

        } catch (SQLException e) {
            if (e.getMessage().toLowerCase().contains("duplicate")) {
                Utils.print("Row " + entry.toString() + " is a Duplicate. Dropping");
                Utils.print(e.getMessage() + "\n");
            } else {
                Utils.print("Error inserting row " + entry.toString() + " to Table");
            }
        }

    }

    public void printTable(int numOfRows) {
        DBTablePrinter.printTable(con, tableName, numOfRows);
    }

    public java.sql.Timestamp javaUtilDateToSQLTimestamp(java.util.Date jDate) {
        if (jDate == null) {
            return null;
        }
        return new java.sql.Timestamp(jDate.getTime());
    }

    public void connect() {
        Statement stmt = null;
        try {
            // Call the Oracle driver
            //
            // String odbcDriver = "sun.jdbc.odbc.JdbcOdbcDriver";
            String odbcDriver = "com.mysql.jdbc.Driver";
            Class.forName(odbcDriver);

            try {
                //Open a connection. I don't provide DB name because it doesn't exist yet.
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=");
                stmt = con.createStatement();
            } catch (SQLException e) {
                Utils.print("Failed to connect to mysql with user name: 'root' and password: ''");
                System.exit(1);
            }
            try {
                // Drop DB if Exist (to recreate with each run)
                stmt.executeUpdate("DROP DATABASE " + DBName);
            } catch (SQLException e) {
                // ignore. always suppose to happen.
            }
            try {
                stmt.executeUpdate("CREATE DATABASE " + DBName);

            } catch (SQLException e) {
                Utils.print("Failed to create DB.");
                System.exit(1);
            }
            try {
                // Get new connection to the new DB.
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/BITEMPORALDB?user=root&password=");

            } catch (SQLException e) {
                Utils.print("Failed to connect to the driver with new DB");
                System.exit(1);
            }
        } catch (Exception e) {
            System.exit(1);
        } finally {
            try {
                if (null != stmt) {
                    stmt.close();
                }
            } catch (SQLException e) {
            }
        }

    }

    public void createTable() {
        Statement statement = null;
        try {
            // Get a statement object from the connection
            //
            statement = con.createStatement();
        } catch (SQLException e) {
            Utils.print("Failed To get Statement from connection before creating Table");
            System.exit(1);
        }
        try {
            // Droping a the table books if it is exists so that later we
            // can create a new table books. If the table does not exists
            // then the dropping will cause an exception to be thrown.
            // The exception is caught so that nothing happens.
            //
            statement.executeUpdate("DROP TABLE " + tableName);
        } catch (SQLException e) {
            // Ignore. Will always happen on rerun.
        }

        try {
            // Creating a table
            String createBooks = "CREATE TABLE " + tableName + " ("
                    + "first_name varchar(30) not null, "
                    + "last_name varchar(30) not null, "
                    + "parameter_name varchar(60) not null, "
                    + "value varchar(50) not null,"
                    + "unit varchar(20) not null, "
                    + "valid_start_time timestamp, "
                    + "valid_stop_time timestamp, "
                    + "transaction_time timestamp not null, "
                    + "transaction_stop_time timestamp null," // allow null values
                    // Set keys
                    + "primary key (first_name, last_name, parameter_name, valid_start_time, valid_stop_time, transaction_time)"
                    + ")";
            statement.executeUpdate(createBooks);

        } catch (SQLException e) {
            Utils.print("Failed to Create Table");
            System.exit(1);
        }
    }

    public void finish() {
        try {
            con.close();
        } catch (SQLException e) {
            // not too bad. ignore.
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitemporaldb;

import static bitemporaldb.UI.*;
/**
 *
 * @author gilmo
 */
public class Main {
        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DB db = new DB();
        db.connect();
        db.createTable();
        UI.InitUI();
        
        int c = ILLEGAL_CHOICE;
        while(c != QUIT) {
            c = UI.getUserChoice();
            switch(c) {
                case INSERT_TO_TABLE_FROM_FILE : {
                    String filename = UI.getFileName();
                    Utils.print("Inserting To Table From File " + filename);
                    db.InsertToTableFromFile(filename);
                    break;
                }
                case INSERT_TO_TABLE_MANUALLY: {
                    MedicalTableEntry entry = UI.getEntryFromUser();
                    db.insertEntry(entry);
                    break;
                }
                case PRINT_MED_TABLE: {
                    db.printTable(db.MedRecordsSize);
                    break;
                }
                case QUIT: {
                    Utils.print("Goodbye");
                    db.finish();
                    UI.finish();
                    break;
                    
                }
                default: {
                    
                    break;
                }
            }
        }
              
    
    }
    
  
}
